<?php

namespace Mush\Player\Enum;

class EndCauseEnum
{
    public const ASSASSINATED = 'assassinated';
    public const DEPRESSION = 'depression';
}
