<?php

namespace Mush\Player\Enum;

class GameStatusEnum
{
    public const CURRENT = 'current';
    public const FINISHED = 'finished';
}
