<?php

namespace Mush\Game\Event;

class DayEvent extends AbstractTimeEvent
{
    public const NEW_DAY = 'new.day';
}
