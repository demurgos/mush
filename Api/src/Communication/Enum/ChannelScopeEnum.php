<?php

namespace Mush\Communication\Enum;

class ChannelScopeEnum
{
    public const PUBLIC = 'public';
    public const PRIVATE = 'private';
    public const MUSH = 'mush';
}
