const COFFEE = "coffee";
const PLASTIC_SCRAP = "plastic_scraps";
const METAL_SCRAP = "metal_scraps";
const BACTA = "bacta";
const CREEPNUT = "creepnut";
const SNIPER_HELMET_BLUEPRINT = "sniper_helmet_blueprint";
const SNIPER_HELMET = "sniper_helmet";
const APPRENTON_PILOTE = "apprenton_pilot";
const STANDARD_RATION = "standard_ration";
const COOKED_RATION = "cooked_ration";
const MAD_KUBE = "mad_kube";
const MICROWAVE = "microwave";
const SUPERFREEZER = "superfreezer";
const HYDROPOT = "hydropot";
const BANANA_TREE = "banana_tree";
const PLASTENITE_ARMOR = "plastenite_armor";
const CAMERA = "camera";
const EXTINGUISHER = "extinguisher";
const DUCT_TAPE = "duct_tape";
const BLASTER = "blaster";
const HACKER_KIT = "hacker_kit";
const GRENADE = "grenade";
const QUADRIMETRIC_COMPASS = "quadrimetric_compass";
const ADJUSTABLE_WRENCH = "adjustable_wrench";
const APRON = "stainproof_apron";
const BLOCK_POST_IT = "block_of_post_it";
const ROPE = "rope";
const DRILL = "drill";
const KNIFE = "knife";
const GLOVES = "protective_gloves";
const SOAP = "soap";
const TABULATRIX = "tabulatrix";


export const itemEnum = {
    [COFFEE]: {
        'image': require('@/assets/images/items/coffee.jpg')
    },
    [METAL_SCRAP]: {
        'image': require('@/assets/images/items/metal_scraps.jpg')
    },
    [PLASTIC_SCRAP]: {
        'image': require('@/assets/images/items/plastic_scraps.jpg')
    },
    [APPRENTON_PILOTE]: {
        'image': require('@/assets/images/items/book.jpg')
    },
    [BACTA]: {
        'image': require('@/assets/images/items/drug_8.jpg')
    },
    [CREEPNUT]: {
        'image': require('@/assets/images/items/fruit01.jpg')
    },
    [SNIPER_HELMET_BLUEPRINT]: {
        'image': require('@/assets/images/items/blueprint.jpg')
    },
    [SNIPER_HELMET]: {
        'image': require('@/assets/images/items/aiming_helmet.jpg')
    },
    [STANDARD_RATION]: {
        'image': require('@/assets/images/items/ration_0.jpg')
    },
    [COOKED_RATION]: {
        'image': require('@/assets/images/items/ration_1.jpg')
    },
    [MAD_KUBE]: {
        'image': require('@/assets/images/items/mad_kube.jpg')
    },
    [MICROWAVE]: {
        'image': require('@/assets/images/items/microwave.jpg')
    },
    [SUPERFREEZER]: {
        'image': require('@/assets/images/items/freezer.jpg')
    },
    [HYDROPOT]: {
        'image': require('@/assets/images/items/tree_pot.jpg')
    },
    [BANANA_TREE]: {
        'image': require('@/assets/images/items/fruit_tree00.jpg')
    },
    [PLASTENITE_ARMOR]: {
        'image': require('@/assets/images/items/plastenite_armor.jpg')
    },
    [CAMERA]: {
        'image': require('@/assets/images/items/camera.jpg')
    },
    [EXTINGUISHER]: {
        'image': require('@/assets/images/items/extinguisher.jpg')
    },
    [DUCT_TAPE]: {
        'image': require('@/assets/images/items/duck_tape.jpg')
    },
    [BLASTER]: {
        'image': require('@/assets/images/items/blaster.jpg')
    },
    [HACKER_KIT]: {
        'image': require('@/assets/images/items/hacker_kit.jpg')
    },
    [GRENADE]: {
        'image': require('@/assets/images/items/grenade.jpg')
    },
    [QUADRIMETRIC_COMPASS]: {
        'image': require('@/assets/images/items/quad_compass.jpg')
    },
    [ADJUSTABLE_WRENCH]: {
        'image': require('@/assets/images/items/wrench.jpg')
    },
    [APRON]: {
        'image': require('@/assets/images/items/apron.jpg')
    },
    [BLOCK_POST_IT]: {
        'image': require('@/assets/images/items/postit_bloc.jpg')
    },
    [ROPE]: {
        'image': require('@/assets/images/items/rope.jpg')
    },
    [DRILL]: {
        'image': require('@/assets/images/items/driller.jpg')
    },
    [KNIFE]: {
        'image': require('@/assets/images/items/knife.jpg')
    },
    [GLOVES]: {
        'image': require('@/assets/images/items/protection_gloves.jpg')
    },
    [SOAP]: {
        'image': require('@/assets/images/items/soap.jpg')
    },
    [TABULATRIX]: {
        'image': require('@/assets/images/items/printer.jpg')
    },
}